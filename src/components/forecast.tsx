﻿import {DayModel} from "../data/DayClass";
import {WeatherDay} from "./weather-day";
import {ForecastDay} from "./forecast-day";
import {useState} from "react";

export const Forecast: React.FC<ForecastProps> = (props) => {
    return (
        <div className="forecast">
            {props.days.map((value => {
                return (
                    <ForecastDay day={value} selectedId={props.selectedId} setSelectedId={(id => props.setSelectedId(id))}/>
                )
            }))}
        </div>
    )
}

interface ForecastProps {
    days: DayModel[];
    selectedId: string;
    setSelectedId: (id: string) => void
}