﻿import {DayModel, DayNames} from "../data/DayClass";

export const ForecastDay: React.FC<ForecastDayProps> = (props) => {
    
    const {day, selectedId} = {...props}
    let icon = "day " + props.day.type;
    
    if (day.id == selectedId) {
        icon += " selected"
    }
    
    const dayDate = new Date(day.day)
    return (
        <div key={day.id} className={icon} onClick={() => props.setSelectedId(day.id)}>
            <p>{DayNames[dayDate.getDay() - 1]}</p>
            <span>{day.temperature}</span>
        </div>
    )
}

interface ForecastDayProps {
    day : DayModel;
    selectedId: string;
    setSelectedId: (id: string) => void
}