﻿import {DayModel, DayNames, MonthNames} from "../data/DayClass";
import React, {useContext} from "react";
import {WeatherContext, WeatherProviderShape} from "../lib/WeatherContext";
import {ensure} from "../lib/helpers";

export const WeatherDay: React.FC = () => {
    const {daysStore} = useContext<WeatherProviderShape>(WeatherContext);
    const day: DayModel = ensure(daysStore.days.find(d => d.id === daysStore.selectedDayId));
    const icon = "icon " + day.type;
    const dayDate = new Date(day.day)
    return (
        <>
            <div className="head">
                <div className={icon}/>
                <div className="current-date">
                    <p>{DayNames[dayDate.getDay() - 1]}</p>
                    <span>{dayDate.getDate()} {MonthNames[dayDate.getMonth()]}</span>
                </div>
            </div>
            <div className="current-weather">
                <p className="temperature">{day.temperature}</p>
                <p className="meta">
                    <span className="rainy">%{day.rain_probability}</span>
                    <span className="humidity">%{day.humidity}</span>
                </p>
            </div>
        </>
    )
}
