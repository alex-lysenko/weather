/* Core */
import waait from 'waait';
import {DayModel} from "../data/DayClass";

export const WEATHER_API_URL = process.env.REACT_APP_WEATHER_API_URL;

export const api = {
    async getWeather() : Promise<DayModel[]> {
        await waait(1000);

        return fetch(`${WEATHER_API_URL}`, {method: 'GET'}).then(response => {
            if (!response.ok) {
                throw new Error(response.statusText)
            }
            return response.json();
        })
            .then(data => { /* <-- data inferred as { data: T }*/
                return data.data
            })
    }
};
