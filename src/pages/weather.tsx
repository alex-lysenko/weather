﻿import {WeatherDay} from "../components/weather-day";
import {Forecast} from "../components/forecast";
import {Filter} from "../components/filter";
import data from "../mock-data/forecast.json"
import {useContext, useState} from "react";
import {ensure} from "../lib/helpers";
import {WeatherContext, WeatherProvider, WeatherProviderShape} from "../lib/WeatherContext";
import { observer } from 'mobx-react-lite'

export const WeatherPage: React.FC = observer(() => {
    const {daysStore} = useContext<WeatherProviderShape>(WeatherContext)
    return (
        <main>
            <Filter/>
            <WeatherDay/>
            <Forecast days={daysStore.days.slice(0,7)} selectedId={daysStore.selectedDayId} setSelectedId={(id => daysStore.setSelectedDayId(id))}/>
        </main>
    )
})

