﻿import {makeAutoObservable} from 'mobx'
import {DayModel} from "../data/DayClass";

export class DaysStore {
    selectedDayId: string = '';

    days: DayModel[] = [];

    constructor() {
        makeAutoObservable(this)
    }

    setSelectedDayId(selectedDayId: string) {
        this.selectedDayId = selectedDayId;
    }

    setDays(days: DayModel[]) {
        this.days = days;
    }
}

export const dayStore = new DaysStore();