﻿import React, {createContext, SetStateAction, useState} from "react";
import data from "../mock-data/forecast.json"
import {DayModel} from "../data/DayClass";
import {useQuery} from "react-query";
import {api, WEATHER_API_URL} from "../api";
import {DaysStore, dayStore} from "../stores/daysStore";

export const WeatherContext = createContext<WeatherProviderShape>({
    daysStore : dayStore
});

export const WeatherProvider: React.FC = props => {

    const query = useQuery<DayModel[]>('days', api.getWeather);
    let d = query.data ? query.data : data;

    console.log(d)

    dayStore.setDays(d);
    dayStore.setSelectedDayId(d[0].id);

    return <WeatherContext.Provider value={{
        daysStore : dayStore
    }}>{props.children}</WeatherContext.Provider>
}

export type WeatherProviderShape = {
    daysStore: DaysStore
}