import {WeatherPage} from "./pages/weather";
import React from "react";
import {WeatherProvider} from "./lib/WeatherContext";
import {QueryClientProvider} from "react-query";
import {queryClient} from "./api/queryClient";


export const App: React.FC = () => {
    return (
        <QueryClientProvider client={queryClient}>
            <WeatherProvider>
                <WeatherPage/>
            </WeatherProvider>
        </QueryClientProvider>
    );
};
